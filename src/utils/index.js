/*
 * @Description: 工具方法
 * @Author: Haor
 * @Date: 2022-01-10 10:32:00
 * @LastEditTime: 2022-01-11 19:10:08
 * @LastEditors: Please set LastEditors
 */

/**
 * 数据类型
 * @param {*} data
 * @param {String} type
 * @returns
 */
export const isType = (data, type) => {
    const dataType = {}.toString.call(data).slice(8, -1).toLowerCase()
    return type === dataType
}

/**
 * 获取数据
 * @param {Object|Array} source 资源
 * @param {Array|String} path 路径  例：user.job.name 或 [‘user’, 'job', 'name']
 * @param {*} defaultValue 默认值
 * @param {Boolean} isDeep 是否为深度
 * @return {*}
 */
export const dGet = (source, path, defaultValue, isDeep) => {
    try {
        if (!source) return defaultValue
        if (!path || (Array.isArray(path) && path.length === 0)) return defaultValue || source
        var paths = Array.isArray(path) ? path : path.replace(/\[(\d+)\]/g, '.$1').split('.')
        var res = isDeep ? JSON.parse(JSON.stringify(source)) : source
        var toNumber = function (d) {
            return isNaN(Number(d)) ? d : Number(d)
        }
        for (var i = 0, len = paths.length; i < len; i++) {
            var key = toNumber(paths[i])
            res = res[key] //记录当前的
            if ([undefined].indexOf(res) > -1) {
                return defaultValue
            }
        }
        return res
    } catch (e) {
        return defaultValue
    }
}

/**
 * 深度拷贝
 * @param {Object|Array} obj
 * @return {Object|Array}
 */
export const deepClone = obj => {
    let objClone = Array.isArray(obj) ? [] : {}
    if (obj && typeof obj === 'object') {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                //判断ojb子元素是否为对象，如果是，递归复制
                if (obj[key] && typeof obj[key] === 'object') {
                    objClone[key] = deepClone(obj[key])
                } else {
                    //如果不是，简单复制
                    objClone[key] = obj[key]
                }
            }
        }
    }
    return objClone
}

/**
 * 获取对象key
 * @param {Object} obj 对象
 * @param {Function} cb 回调函数
 * @param {*} defaultValue 默认值
 * @return {String} key
 */
export const findObjectKey = (obj, cb, defaultValue) => {
    for (let key in obj) {
        const res = cb ? cb(obj[key], key) : false
        if (res) {
            return key
        }
    }
    return defaultValue;
}
