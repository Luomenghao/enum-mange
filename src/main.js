/*
 * @Description: 
 * @Author: Haor
 * @Date: 2022-01-10 10:59:40
 * @LastEditTime: 2022-01-11 19:23:00
 * @LastEditors: Please set LastEditors
 */

import { orderStateEnum } from './common/enums/order.js' //引入你对应的枚举模块

// 默认订单状态状态
const orderState = 1

console.log(orderStateEnum)

// 获取多个枚举值,方法 getValMap(), 再配合es6 解构，是不是美滋滋？？
const { AWAIT_SEND_GOODS, IN_TRANSIT, RECEIVED } = orderStateEnum.getValMap()
console.log(AWAIT_SEND_GOODS, IN_TRANSIT, RECEIVED) // 0, 1

// 获取枚举值，如：获取订单待发货枚举值
console.log("代发货：--->", orderStateEnum.getVal('AWAIT_SEND_GOODS')) // 1

// 获取多个枚举值,方法 getValList(key1, key2, ...keyn)
console.log("代发货、运输中：--->", orderStateEnum.getValList('AWAIT_SEND_GOODS', 'IN_TRANSIT')) // [0, 1]

// 验证当前订单状态是否为（已收货）
console.log("订单是否已收货：--->", orderStateEnum.check(orderState, 'RECEIVED')) // false


// 通过枚举值，获取枚举名称
const enumKeyName = orderStateEnum.getNameByValue(1)
console.log("枚举key：--->", enumKeyName) // IN_TRANSIT

// 通过枚举值，获取枚举值名称, 获取状态为1的中文
const enumValueName = orderStateEnum.getValName(1)
console.log("枚举值名称：--->", enumValueName) // 运输中


