import { createEnum } from '../../enum-class.js' //枚举容器

// 订单状态枚举
const orderStateEnum = createEnum({
    AWAIT_SEND_GOODS: [0, '待发货'],
    IN_TRANSIT: [1, '运输中'],
    RECEIVED: [2, '已收货']
})

export { orderStateEnum }