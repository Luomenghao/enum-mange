import { createEnum } from '../../enum-class.js' //枚举容器

// 用户登录状态
const userLoginStateEnum = createEnum({
    OFF_LINE: [0, '离线'],
    ON_LINE: [1, '在线'],
    STEALTH: [2, '隐身'],
    NOT_BOTHER: [3, '请勿打扰']
})

export { userLoginStateEnum }
