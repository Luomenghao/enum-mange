/*
 * @Description: 
 * @Author: Haor
 * @Date: 2022-01-10 10:58:12
 * @LastEditTime: 2022-01-10 12:55:34
 * @LastEditors: Please set LastEditors
 */
const { resolve } = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode:'development', //开发环境
  entry: {
    path: resolve(__dirname, 'src/main.js')
  },
  output: {
    path: resolve(__dirname,'./dist'), //这里必须是绝对路径
    filename: 'app.bundle.js'
  },
  // devServer: {
  //   static: './dist', // 本地服务器所加载文件的目录
  //   host:"0.0.0.0", // 指定地址  可以是0.0.0.0也可以是localhot
  //   port: 19090,
  //   inline: true, //实时刷新
  //   hot: true,  // 文件修改后实时刷新
  //   historyApiFallback: true //不跳转
  // },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html', //生成的新的html文件所依赖的模板
      filename: 'index.html' //生成的新的html文件的名字
    })
  ]
}