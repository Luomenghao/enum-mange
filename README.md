
# 如何更好的管理项目中的枚举？

> 什么是枚举？ 1.枚举key，2.枚举值，3.枚举值描述

[gitee源码地址](https://gitee.com/Luomenghao/enum-mange)

## 1. ❌做法
### 1.是这样吗？
```javascript
  // 这种？？？
  const userState = {
    0: '离线',
    1: '在线',
    3: '隐身'
  }
```


### 2.还是这样？
```javascript
 const loginState = 0;
 const getUserState = (state) => {
    if (state === 0) {
     	return '离线'
    } else if (state === 1) {
      	return '在线'
    } else if (state === 2) {
      	return '隐身'
    }
 }
// 逻辑判断
if (loginState === 1 || loginState === 2) {
	// do sth...
}
```

### 3.痛点
 - [x] 枚举东西一旦多了起来，很难维护、管理;
 - [x] 在代码中写0，1，2，3...可读性很差，同事看了无法理解意思；
 - [x] 多个模块使用，定义多套，如果后段更改了值，那么前端就好玩了；


## 2.✅做法

### 统一管理
以 Vue 项目为例，枚举统一放在, ./src/common/enums/xxxx.js
```bash
|-- src 
    |-- common
    |   |   |-- enums
    |   |   |   |-- common.js   # 公共模块
    |   |   |   |-- user.js     # 用户模块
    |   |   |   |-- order.js    # 订单模块
    |   |   |   |-- auth.js     # 权限模块
    |   |   |   |-- ...         # 更多
    |--
```

### 统一按需访问
例子1，以 **Vue** 为例，如果在xxx页面需要使用枚举
```javascript
<script>
	import { loginStateEnum } from '@enums/user' //用户模块枚举，注意：使用了别名
	export default {
		data() {
			return {
				// 登录状态
				loginState: 1
			}
		},
    	methods: {
	    	// 获取用户状态
	      	getUserState() {
	        	// 解构出来的枚举
	        	const { ON_LINE, OFF_LINE } = loginStateEnum.getValMap()
	        	const { loginState } = this
	        	if (loginState === ON_LINE) {
	        		return '用户已上线呀~'
				} else if (loginState === OFF_LINE) {
					return '用户下线了~~'
				}
	    	}
	    }
	}
</script>
```

例子**2**
```javascript

import { orderStateEnum } from './common/enums/order.js' // 推荐使用按需引入你对应的枚举模块


// 获取枚举值，如：获取订单待发货枚举值
console.log("代发货：--->", orderStateEnum.getVal('AWAIT_SEND_GOODS')) // 1

// 获取多个枚举值,方法 getValList(key1, key2, ...keyn)
console.log("代发货、运输中：--->", orderStateEnum.getValList('AWAIT_SEND_GOODS', 'IN_TRANSIT')) // [0, 1]

// 获取多个枚举值,方法 getValMap(), 再配合es6 解构，是不是美滋滋？？
const { AWAIT_SEND_GOODS, IN_TRANSIT, RECEIVED } = orderStateEnum.getValMap()
console.log(AWAIT_SEND_GOODS, IN_TRANSIT, RECEIVED) // 0, 1

// （获取枚举名、枚举描述、通过枚举值获取枚举描述）等更多方法请看下面文档
```

## 2.如何使用？
```javascript
// 1.引入枚举包装类
import { EnumWrap } from './enum-class.js' //枚举核心模块


// 2.实例化一个用户登录状态枚举
const userLoginStateEnum = new EnumWrap({
  OFF_LINE: [0, '离线'],
  ON_LINE: [1, '在线'],
  STEALTH: [2, '隐身'],
  NOT_BOTHER: [3, '请勿打扰']
})


// 3.在使用模块导入即可， 比如定义了大模块叫 user.js 
import { userLoginStateEnum } from './src/common/enums/user.js'

// es6 解构出想要的枚举值
const { OFF_LINE, ON_LINE, STEALTH, NOT_BOTHER } = userLoginStateEnum.getValMap()

if (userState === OFF_LINE) {
	console.log('用户离线啦~')
} else if (userState === ON_LINE) {
  	console.log('用户在线啦~')
}
// next do sth....

```

## 2.内置方法
|方法名| 参数 | 返回值|备注
|--|--|--|--|
| getVal(key) | key: 枚举中key   | Number、String单个枚举值
| getValList(key1, key2, ...keyn) | key: 枚举key   | Array，列表枚举值
| getValMap(key1, key2, ...keyn) | key: 枚举key   | Object，对象枚举值{key1: value1, key2: value2} | es6解构使用更佳
| getName(key) | key：枚举key | String，枚举值名字


## 3.备注
建议大家所有枚举放在一个目录统一，如：**./common/enums/模块名.js**
使用地方再去引入 **import { xxxxxEnum } from '@enums/user'**

>Q:为什么要这么做？引入？<br />
A: 其实这样更能看出一个模块到底依赖于外部什么内容，对于后期编码很大的方法，从上往下看就知道改模块依赖于外部xxxx模块；

## 贴图

### 注册地方
![在这里插入图片描述](https://img-blog.csdnimg.cn/ecdf2c00d4784150bc1dd53edf28f588.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASGFvcuWRgA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
### 使用地方
![在这里插入图片描述](https://img-blog.csdnimg.cn/b1cedb69e01547aea950fa25ee637d14.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBASGFvcuWRgA==,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center)
